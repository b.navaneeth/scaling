# Scaling
It is a process of expanding and compressing the system resources to meet user's needs. We required scaling because at the start we don't know how many users will come to our application and in time the users will change if we use scaling we can use resources wisely.  

There are 2 types of scaling:

1. Vertical scaling
1. Horizontal scaling

## Horizontal scaling
When a new server is added to existing servers we call it vertical scaling.
There are multiple servers.
### Pros
* __Load balancing:__ The load is distributed to all servers.
* __Resilient to failures:__ If a server goes down the rest servers can do the job. Users will have no issues.
* __Downtime:__ Adding more servers we are not limited to a single unit so the downtime is less.
* __Concurrency:__ It is improver as there are many servers.
* __Scales well:__ Resources are unlimited we can as many servers as we want.
### Cons
* __Increased cost:__ Additional hardware/servers require increased infrastructure & maintenance costs.
* __More Time:__ It takes more time and difficult to be implemented.
* __Communication:__ They are network calls they are slow as the server to server communication is external.
* __Data:__ Here the data is inconsistent.

## Vertical scaling
This involves the addition of more processing power and storage, to an existing single server. Used for low users applications.

### Pros
* __Seamless upgrade:__ We can add and reduce is done very easily and takes less time.
* __Cost:__ Maintenance cost is minimum.
* __Data:__ Data is consistent here as there is a single server.
* __Communication:__ With Inter-process communication is done easily and fast.
### Cons
* __Single point:__ As there is only one server if it fails users cant use applications.
* __Scales:__ It doesn't scale well because the resources that we can add to a single system are limited. It scales up to a limit only. 
* __Concurrency:__ It is done using multi-threading not as fast as horizontal scaling.
* __Load balancing:__ It is not there.

> In the real-world we use a combination of both.
___

## Load balancing:
It is a process of efficiently distributing incoming traffic. It tells what server to what activity.
It uses different algorithms to distribute
they are:
* Round Robin: 
* Least Connections.
* Least Time.
* Hash.
* IP Hash.
* Random with Two Choices.
#### Benefits of Load Balancing
* Reduced downtime
* Scalable
* Redundancy
* Flexibility
* Efficiency

## References
* https://www.geeksforgeeks.org/horizontal-and-vertical-scaling-in-databases/
* https://www.section.io/blog/scaling-horizontally-vs-vertically/
* https://www.nginx.com/resources/glossary/load-balancing/
* https://dzone.com/refcardz/scalability?chapter=1
* https://www.stratoscale.com/blog/cloud/implementing-horizontal-vertical-scalability-cloud-computing/